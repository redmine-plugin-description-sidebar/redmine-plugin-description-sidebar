Redmine::Plugin.register :description_sidebar do
  name "Description Sidebar plugin"
  author "ClearCode Inc."
  description "This plugin provides ability to embed the contents of the \"description\" field of the project to the sidebar area of issues pages"
  version "0.0.2"
  url "https://gitlab.com/redmine-plugin-description-sidebar/redmine-plugin-description-sidebar"
  author_url "https://www.clear-code.com/"
end

# Load
DescriptionSidebarHookListener
